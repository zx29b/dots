" dark text
set bg=light
" search
set hlsearch
set incsearch
" Use sys clipboard
set clipboard=unnamedplus
" sets tabs to 4 spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
" converts tabs to spaces
set expandtab
set autoindent
set fileformat=unix

"compatablity
set nocompatible
syntax on
set encoding=utf-8
"numbering
set number relativenumber
"enables autocompletion ctrl+n to activate
set wildmode=longest,list,full
"disables auto-commenting on new line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
"splits vertically
set splitbelow splitright
" shortcuts split nav
    map <C-h> <C-w>h
    map <C-j> <C-w>j
    map <C-k> <C-w>k
    map <C-l> <C-w>l

    map <S-Insert> <C-i>
"get colors form Xresources
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
